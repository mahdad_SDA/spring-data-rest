package com.sda.springdata.tutorials.demo.springData.repository;

import com.sda.springdata.tutorials.demo.springData.domain.Company;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface CompanyRepository extends CrudRepository<Company,Long>{

    @Query("FROM Company cmp WHERE LOWER(cmp.taxCode) = LOWER(:taxCode)")
    public Company findByTaxCode(@Param("taxCode") String taxCode);

}
