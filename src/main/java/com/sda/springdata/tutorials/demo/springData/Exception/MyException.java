package com.sda.springdata.tutorials.demo.springData.Exception;

public class MyException extends Throwable {
    public MyException(String message) {
        super(message);
    }
}
