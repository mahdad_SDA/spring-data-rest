package com.sda.springdata.tutorials.demo.springData.controller;

import com.sda.springdata.tutorials.demo.springData.Exception.MyException;
import com.sda.springdata.tutorials.demo.springData.domain.Company;
import com.sda.springdata.tutorials.demo.springData.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/company")
public class CompanyController{

    private final CompanyRepository companyRepository;

    @Autowired
    public CompanyController(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @DeleteMapping(value = "{id}/delete")
    public ResponseEntity delete(@PathVariable Long id){
//      Company newCompany = companyRepository.findById(id).get();
        HttpStatus httpStatus  = HttpStatus.OK;
        try{
            companyRepository.deleteById(id);
            return new ResponseEntity(httpStatus);
        }catch (Exception e){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/add")
    public Company addCompany(@RequestBody Company company){
        return companyRepository.save(company);
    }

    @GetMapping(value = "/")
    public String helloCompany(){
        return "Hello Timur";
    }

    @GetMapping(value = "/{id}")
    public Company findById(@PathVariable Long id) throws MyException {
        if(companyRepository.findById(id).isPresent()){
            return companyRepository.findById(id).get();
        }else{
            throw new MyException("my beautiful exception");
        }
    }

    @PutMapping(value = "{id}/update")
//    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity update(@PathVariable Long id, @RequestBody Company company) throws Exception{
        if (companyRepository.findById(id).isPresent()){
            Company company1 = companyRepository.findById(id).get();
            company1.setAddress(company.getAddress());
            company1.setName(company.getName());
            company1.setTaxCode(company.getTaxCode());
            Company updatedCompany = companyRepository.save(company1);
            return new ResponseEntity(updatedCompany, HttpStatus.OK);
        } else {
            throw new Exception();
        }
    }

//    @GetMapping(value = "/{taxCode}")
//    public Company findByTaxCode(@PathVariable String tax){
//        return companyRepository.findByTaxCode(tax);
//    }
}
