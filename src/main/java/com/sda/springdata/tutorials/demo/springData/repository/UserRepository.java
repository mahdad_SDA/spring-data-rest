package com.sda.springdata.tutorials.demo.springData.repository;

import com.sda.springdata.tutorials.demo.springData.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
}
